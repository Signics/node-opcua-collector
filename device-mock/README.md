### 概要
- 装置モック イメージ 
### Install
```
docker-compose build
docker-compose run device-mock /bin/sh
npm install
```
### ビルド
```
docker-compose run --service-ports device-mock npm run build 
```

### サーバー起動手順
```
docker-compose run --service-ports device-mock npm run start
```

- サーバーアドレス
	- <opc.tcp://localhost:26543>
	> opcuaクライアントで接続確認可能

- Free OPCUAクライアント
	- [UAExpert](https://www.unified-automation.com/downloads/opc-ua-clients.html)
		- ユーザー登録が必要
    - [opcua-client-gui](https://github.com/FreeOpcUa/opcua-client-gui)

### デバッグ起動手順	
```
docker-compose run --service-ports device-mock npm run debug 
```
