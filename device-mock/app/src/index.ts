import {DataType, Variant, StatusCodes, OPCUAServer, AddressSpace, construct_demo_alarm_in_address_space} from "node-opcua";

main();

function main() {
	const server = new OPCUAServer({
		port:26543,
		resourcePath: "/UA/SampleServer",
		buildInfo: {
			productName:"MySampleServer1",
			buildNumber:"7650",
			buildDate: new Date(2014, 5, 2)
		}
	});

	server.initialize( function (){
    	console.log("initialized");

	    construct_my_address_space(server);

	    server.start(function () {
	        console.log("Server is now listening ... ( press CTRL+C to stop) ");
	        server.endpoints[0].endpointDescriptions().forEach(function (endpoint:any) {
        	    console.log(endpoint.endpointUrl, endpoint.securityMode.toString(), endpoint.securityPolicyUri.toString());
        	});
	    });
	});
}
function construct_my_address_space(server:any) {
    const addressSpace : AddressSpace = server.engine.addressSpace;
	const namespace = addressSpace!.getOwnNamespace();
    
    const myDevice = namespace.addFolder("ObjectsFolder", {
        browseName: "Device"
	});
    const variable1 = 10.0;

    server.barrel = namespace.addObject({
        componentOf: myDevice,
        browseName:"Barrel",
        description: "The Object representing the Barrel"
    })
    server.temp = namespace.addVariable({
        nodeId: "s=Temperature",
        browseName: "Temperature",
        dataType: "Double",
        eventSourceOf: server.barrel,
        propertyOf: server.barrel,
        value: {
            get: () => {
                const t = new Date().getSeconds()/10;
				const value = variable1 + 10.0 * Math.sin(t);
				return new Variant({dataType: DataType.Double, value:value});
			}
        }
    });
    
    const exclusiveLimitAlarmType = addressSpace.findEventType("ExclusiveLimitAlarmType");
    
    const tempCondition = namespace!.instantiateExclusiveLimitAlarm(exclusiveLimitAlarmType!,{
        browseName:      "TempratureCondition",
        conditionName:      "TempratureCondition",
        componentOf: server.barrel,
        conditionSource: server.temp,
        optionals: [
            "ConfirmedState", "Confirm" // confirm state and confirm Method
        ],
        inputNode:       server.temp,   // the variable that will be monitored for change
        highHighLimit:   18,
        highLimit:       16,
        lowLimit:        4,
        lowLowLimit:     2 
     });

     server.tempCondition = tempCondition;
}

