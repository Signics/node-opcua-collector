### 概要
- Edge層管理用リポジトリ 

### 構成 
- 本リポジトリ複数のサービスで構成されている。
	- [device-mock](./device-mock) : 装置モック
    - [data-collector](./data-collector)  : データコレクタ

### Install
```
docker-compose build
docker-compose run <service> /bin/sh
npm install
```
### ビルド
```
docker-compose run --service-ports <service> npm run build 
```

### サービス起動	
```
docker-compose up -d 
```
