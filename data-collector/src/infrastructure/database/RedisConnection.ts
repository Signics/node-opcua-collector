import { INoSqlConnection } from "../../interface/database/INoSqlConnection";
//import { RedisClient } from "redis";
import redis from 'redis';

export class RedisConnection implements INoSqlConnection {
    private client: redis.RedisClient;
    constructor(){
        this.client = redis.createClient({host:"dumped-device-log",port:6379});
    }
    setLog(key:any, value:any):any{
        console.log(key,":", value);
        this.client.set(key, value);
    }
}