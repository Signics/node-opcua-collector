import {IDeviceConnection} from "../../interface/devices/IDeviceConnection"
import { OPCUAClient,AttributeIds, ClientSession, DataValue, ClientSubscription, ReadValueIdLike, ClientMonitoredItem, TimestampsToReturn, Variant, constructEventFilter, DataType } from 'node-opcua';

export class OpcuaConnection implements IDeviceConnection {
    private _client : OPCUAClient | undefined = undefined;
    private _session : ClientSession | undefined  = undefined;
    private _subscription : ClientSubscription | undefined = undefined;
    async connect(url:string):Promise<any> {
        try{
            this._client = OPCUAClient.create({endpoint_must_exist:false});
            await this._client.connect(url);

            this._session = await this._client.createSession();
        } catch (err) {
            console.log("Error !!",err);
            throw err;
        }
    }

    async read(nodeId:string){
        let value = 0;
        const maxAge = 0;
        const nodeToRead = {
            nodeId: nodeId,
            attributedId: AttributeIds.Value 
        };
        if(this._session){
            const dataValue = await this._session.read(nodeToRead, maxAge);
            value = dataValue.value.value;
        }
        return value;
    }

    private createSubscription() {
        if(!this._session) return;
        const subscription = ClientSubscription.create(this._session, {
            requestedPublishingInterval: 1000,
            requestedLifetimeCount: 100,
            requestedMaxKeepAliveCount: 10,
            maxNotificationsPerPublish: 100,
            publishingEnabled: true,
            priority: 10
        });
          
        subscription
          .on("started", function() {
            console.log(
              "subscription started for 2 seconds - subscriptionId=",
              subscription.subscriptionId
            );
          })
          .on("keepalive", function() {
            console.log("keepalive");
          })
          .on("terminated", function() {
            console.log("terminated");
          });
        
        this._subscription = subscription;
    }

    async subscriptionValue(nodeId:string, field:string, callback:(timestamp:number, jsonValue:any) => void) {
        if(!this._session){
            return;
        }
        if(!this._subscription) {
            this.createSubscription();
        }
         
        const itemToMonitor: ReadValueIdLike = {
          nodeId: nodeId,
          attributeId: AttributeIds.Value
        };

        const parameters = {
          samplingInterval: 100,
          discardOldest: true,
          queueSize: 10
        };
        
        const monitoredItem = ClientMonitoredItem.create(
          this._subscription!,
          itemToMonitor,
          parameters,
          TimestampsToReturn.Both
        );
        
        monitoredItem.on("changed", (dataValue: DataValue) => {
            let timestamp = new Date().getTime();
            let logData = this.logSeriarize(field, dataValue.value);
            callback(timestamp, logData);
        });
    }

    async subscriptionEvent(nodeId:string, fields:Array<string>, callback:(timestamp:number, jsonValue:any) => void) {
        let filter = constructEventFilter(fields);

        const itemToMonitor: ReadValueIdLike = {
          nodeId: nodeId,
          attributeId: AttributeIds.EventNotifier
        };
        const monitoredItem = ClientMonitoredItem.create(
          this._subscription!,
          itemToMonitor,
        {
          samplingInterval:100,
          filter:filter,
          queueSize:10,
          discardOldest:true
        },
        TimestampsToReturn.Both
        );

        monitoredItem.on("changed", (dataValues:Array<Variant>) => {
            let timestamp = new Date().getTime();
            let logData = this.logSeriarize(fields, dataValues);
            callback(timestamp, logData);
        });
    }

    private logSeriarize(fields:string|Array<string>, logs:Variant|Array<Variant>):string{
      let obj: { [x: string]: any; }[] = [];
      if(!Array.isArray(fields)) {
        if(!Array.isArray(logs))
          obj.push({[fields]:logs.toValue()});
      } else {
          fields.forEach((value, index)=>{
            if(Array.isArray(logs)) {
              obj.push({[value]:logs[index].toValue()});
            }
          })
      }
      return JSON.stringify(obj);
    }
}

declare module "node-opcua" {
  interface Variant {
    toValue():any;
  }
}

Variant.prototype.toValue = function() {
    let value;
    switch(this.dataType) {
      case DataType.LocalizedText:
        value = this.value.text;
        break;
      case DataType.Null:
      case DataType.Boolean:
      case DataType.SByte:
      case DataType.Byte:
      case DataType.Int16:
      case DataType.UInt16:
      case DataType.Int32:
      case DataType.UInt32:
      case DataType.Int64:
      case DataType.UInt64:
      case DataType.Float:
      case DataType.Double:
      case DataType.String:
        value = this.value;
        break;
      case DataType.DateTime:
        value = this.value.getTime();
        break;
//      case DataType.Guid:
//      case DataType.ByteString:
//      case DataType.XmlElement:
//      case DataType.NodeId:
//      case DataType.ExpandedNodeId:
//      case DataType.StatusCode:
//      case DataType.QualifiedName:
//      case DataType.ExtensionObject:
//      case DataType.DataValue:
//      case DataType.Variant:
//      case DataType.DiagnosticInfo:
//        break;
    }
    return value;
}