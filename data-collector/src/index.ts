import { OpcuaConnection } from "./infrastructure/devices/OpcuaConnection"
import { DeviceController } from "./interface/controllers/DeviceController"
import { SetDeviceLog } from "./application/usecase/SetDeviceLog";
import { DeviceLogRepository } from "./interface/database/DeviceLogRepository";
import { RedisConnection } from "./infrastructure/database/RedisConnection";
const options:Array<any> = require('/app/conf/setting.json');

options.forEach((option: void) => {
   let deviceConnection = new OpcuaConnection();
   let nosqlConnection = new RedisConnection();
   let usecase = new SetDeviceLog(new DeviceLogRepository(nosqlConnection));
   let controller = new DeviceController(usecase, deviceConnection);

   controller.execute(option);
});