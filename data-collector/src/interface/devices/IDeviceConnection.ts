export interface IDeviceConnection {
    connect(url:string):Promise<any>
    read(nodeId:string):void
    subscriptionValue(nodeId:string, field:string, callback:(timestamp:number, jsonValue:any) => void):void
    subscriptionEvent(nodeId:string, fields:Array<string>, callback:(timestamp:number, jsonValue:any) => void):void
}