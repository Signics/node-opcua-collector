import { ISetDeviceLog } from "../../application/usecase/SetDeviceLog";
import { IDeviceConnection } from "../devices/IDeviceConnection";
import { DeviceLog } from "../../domain/models/DeviceLog";


export class DeviceController {
    private usecase:ISetDeviceLog;
    private deviceConnection:IDeviceConnection;

    constructor(usecase:ISetDeviceLog, deviceConection:IDeviceConnection){
        this.usecase = usecase;
        this.deviceConnection = deviceConection;
    }

    async execute(options:any){
        this.connection(options.url).then(async () =>{
            options.value.forEach((value: { nodeId: string; field: string; })=>{
                this.subscribeValue(value.nodeId, value.field);
            });
            options.event.forEach((value: { nodeId: string; fields: string[]; })=>{
                this.subscribeEvent(value.nodeId, value.fields);
            });
        });
    }

    private async connection(url:string):Promise<any> {
        await this.deviceConnection.connect(url);
    }

    private subscribeValue(nodeId:string, field:string){
        this.deviceConnection.subscriptionValue(nodeId,field,(timestamp:number, jsonValue:any)=>{
            let deviceLog = new DeviceLog(timestamp, jsonValue);
            this.usecase.execute(deviceLog);
        });
    }
    private subscribeEvent(nodeId:string, fields:Array<string>){
        this.deviceConnection.subscriptionEvent(nodeId,fields,(timestamp:number, jsonValue:any)=>{
            let deviceLog = new DeviceLog(timestamp, jsonValue);
            this.usecase.execute(deviceLog);
        });
    }
}