export class DeviceLog {
    private timestamp: number; 
    private jsonValue: any;
    get eventTime(){
        return this.timestamp;
    }

    get logData(){
        return this.jsonValue;
    }

    constructor(eventtime:number, jsonValue:any) {
        this.timestamp = eventtime;
        this.jsonValue = jsonValue;
    }
}