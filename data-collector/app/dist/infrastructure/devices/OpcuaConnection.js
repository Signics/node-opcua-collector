"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DeviceLog_1 = require("../../domain/models/DeviceLog");
const node_opcua_1 = require("node-opcua");
class OpcuaConnection {
    constructor() {
        this._client = undefined;
        this._session = undefined;
        this._subscription = undefined;
    }
    async connect(url) {
        try {
            this._client = node_opcua_1.OPCUAClient.create({ endpoint_must_exist: false });
            await this._client.connect(url);
            this._session = await this._client.createSession();
        }
        catch (err) {
            console.log("Error !!", err);
            throw err;
        }
    }
    async read(nodeId) {
        let value = 0;
        const maxAge = 0;
        const nodeToRead = {
            nodeId: nodeId,
            attributedId: node_opcua_1.AttributeIds.Value
        };
        if (this._session) {
            const dataValue = await this._session.read(nodeToRead, maxAge);
            value = dataValue.value.value;
        }
        return value;
    }
    createSubscription() {
        if (!this._session)
            return;
        const subscription = node_opcua_1.ClientSubscription.create(this._session, {
            requestedPublishingInterval: 1000,
            requestedLifetimeCount: 100,
            requestedMaxKeepAliveCount: 10,
            maxNotificationsPerPublish: 100,
            publishingEnabled: true,
            priority: 10
        });
        subscription
            .on("started", function () {
            console.log("subscription started for 2 seconds - subscriptionId=", subscription.subscriptionId);
        })
            .on("keepalive", function () {
            console.log("keepalive");
        })
            .on("terminated", function () {
            console.log("terminated");
        });
        this._subscription = subscription;
    }
    async subscriptionValue(nodeId, field, callback) {
        if (!this._session) {
            return;
        }
        if (!this._subscription) {
            this.createSubscription();
        }
        // install monitored item
        const itemToMonitor = {
            nodeId: nodeId,
            attributeId: node_opcua_1.AttributeIds.Value
        };
        const parameters = {
            samplingInterval: 100,
            discardOldest: true,
            queueSize: 10
        };
        const monitoredItem = node_opcua_1.ClientMonitoredItem.create(this._subscription, itemToMonitor, parameters, node_opcua_1.TimestampsToReturn.Both);
        monitoredItem.on("changed", (dataValue) => {
            let timestamp = new Date().getTime();
            let logData = this.logSeriarize(field, dataValue.value);
            let deviceLog = new DeviceLog_1.DeviceLog(timestamp, logData);
            callback(deviceLog);
        });
    }
    async subscriptionEvent(nodeId, fields, callback) {
        let filter = node_opcua_1.constructEventFilter(fields);
        const itemToMonitor = {
            nodeId: nodeId,
            attributeId: node_opcua_1.AttributeIds.EventNotifier
        };
        const monitoredItem = node_opcua_1.ClientMonitoredItem.create(this._subscription, itemToMonitor, {
            samplingInterval: 100,
            filter: filter,
            queueSize: 10,
            discardOldest: true
        }, node_opcua_1.TimestampsToReturn.Both);
        monitoredItem.on("changed", (dataValues) => {
            let timestamp = new Date().getTime();
            let logData = this.logSeriarize(fields, dataValues);
            let deviceLog = new DeviceLog_1.DeviceLog(timestamp, logData);
            callback(deviceLog);
        });
    }
    logSeriarize(fields, logs) {
        let obj = [];
        if (!Array.isArray(fields)) {
            if (!Array.isArray(logs))
                obj.push({ [fields]: logs.toValue() });
        }
        else {
            fields.forEach((value, index) => {
                if (Array.isArray(logs)) {
                    obj.push({ [value]: logs[index].toValue() });
                }
            });
        }
        return JSON.stringify(obj);
    }
}
exports.OpcuaConnection = OpcuaConnection;
node_opcua_1.Variant.prototype.toValue = function () {
    let value;
    switch (this.dataType) {
        case node_opcua_1.DataType.LocalizedText:
            value = this.value.text;
            break;
        case node_opcua_1.DataType.Null:
        case node_opcua_1.DataType.Boolean:
        case node_opcua_1.DataType.SByte:
        case node_opcua_1.DataType.Byte:
        case node_opcua_1.DataType.Int16:
        case node_opcua_1.DataType.UInt16:
        case node_opcua_1.DataType.Int32:
        case node_opcua_1.DataType.UInt32:
        case node_opcua_1.DataType.Int64:
        case node_opcua_1.DataType.UInt64:
        case node_opcua_1.DataType.Float:
        case node_opcua_1.DataType.Double:
        case node_opcua_1.DataType.String:
            value = this.value;
            break;
        case node_opcua_1.DataType.DateTime:
            value = this.value.getTime();
            break;
        //      case DataType.Guid:
        //      case DataType.ByteString:
        //      case DataType.XmlElement:
        //      case DataType.NodeId:
        //      case DataType.ExpandedNodeId:
        //      case DataType.StatusCode:
        //      case DataType.QualifiedName:
        //      case DataType.ExtensionObject:
        //      case DataType.DataValue:
        //      case DataType.Variant:
        //      case DataType.DiagnosticInfo:
        //        break;
    }
    return value;
};
//# sourceMappingURL=OpcuaConnection.js.map