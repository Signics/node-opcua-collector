"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const OpcuaConnection_1 = require("./infrastructure/devices/OpcuaConnection");
const DeviceController_1 = require("./interface/controllers/DeviceController");
const SetDeviceLog_1 = require("./application/usecase/SetDeviceLog");
const DeviceLogRepository_1 = require("./interface/database/DeviceLogRepository");
const RedisConnection_1 = require("./infrastructure/database/RedisConnection");
let controller = new DeviceController_1.DeviceController(new SetDeviceLog_1.SetDeviceLog(new DeviceLogRepository_1.DeviceLogRepository(new RedisConnection_1.RedisConnection())), new OpcuaConnection_1.OpcuaConnection());
controller.execute("a");
//const opcua = new OpcuaConnection();
//opcua.connect("opc.tcp://device-mock:26543").then(async () =>{
//    const value = await opcua.read("ns=1;s=Temperature");
//    console.log("data:",value);
//
//    await opcua.subscriptionValue("ns=1;s=Temperature","Temprature",(deviceLog)=>{
//        console.log(deviceLog);
//    });
//    await opcua.subscriptionValue("ns=1;i=1046","Condition Message",(deviceLog)=>{
//        console.log(deviceLog);
//    });
//    var fields = [
//        "ReceiveTime",
//        "Message"
//    ];
//    await opcua.subscriptionEvent("ns=1;i=1001",fields,(deviceLog)=>{
//        console.log(deviceLog);
//    });
//
//});
//# sourceMappingURL=index.js.map