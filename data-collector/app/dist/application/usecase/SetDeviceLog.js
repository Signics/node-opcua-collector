"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SetDeviceLog {
    constructor(deviceLogRepository) {
        this.deviceLogRepository = deviceLogRepository;
    }
    execute(deviceLog) {
        return this.deviceLogRepository.SetDeviceLog(deviceLog);
    }
}
exports.SetDeviceLog = SetDeviceLog;
//# sourceMappingURL=SetDeviceLog.js.map