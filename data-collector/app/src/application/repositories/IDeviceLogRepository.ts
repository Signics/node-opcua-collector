import { DeviceLog } from "../../domain/models/DeviceLog";

export interface IDeviceLogRepository {
    SetDeviceLog(deviceLog:DeviceLog):Promise<any>
}