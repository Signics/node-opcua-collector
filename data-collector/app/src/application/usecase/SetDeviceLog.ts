import { IDeviceLogRepository } from "../repositories/IDeviceLogRepository";
import { DeviceLog } from "../../domain/models/DeviceLog";

export interface ISetDeviceLog {
    execute(deviceLog:DeviceLog):any;
}

export class SetDeviceLog implements ISetDeviceLog {
    private deviceLogRepository:IDeviceLogRepository;

    constructor(deviceLogRepository:IDeviceLogRepository){
        this.deviceLogRepository = deviceLogRepository;
    }

    execute(deviceLog:DeviceLog) {
        return this.deviceLogRepository.SetDeviceLog(deviceLog);    
    }
}