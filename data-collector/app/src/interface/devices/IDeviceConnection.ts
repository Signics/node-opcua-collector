import { DeviceLog } from "../../domain/models/DeviceLog";

export interface IDeviceConnection {
    connect(url:string):Promise<any>
    read(nodeId:string):void
    subscriptionValue(nodeId:string, field:string, callback:(deviceLog:DeviceLog) => void):void
    subscriptionEvent(nodeId:string, fields:Array<string>, callback:(deviceLog:DeviceLog) => void):void
}