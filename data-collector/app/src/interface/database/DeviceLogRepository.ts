import { IDeviceLogRepository } from "../../application/repositories/IDeviceLogRepository";
import { INoSqlConnection } from "./INoSqlConnection";
import { DeviceLog } from "../../domain/models/DeviceLog";

export class DeviceLogRepository implements IDeviceLogRepository {
    private connection:INoSqlConnection;
    constructor(connection:INoSqlConnection) {
        this.connection = connection;
    }
    async SetDeviceLog(deviceLog:DeviceLog): Promise<any> {
        this.connection.setLog(deviceLog.eventTime, deviceLog.logData);
    }
}