import { SetDeviceLog, ISetDeviceLog } from "../../application/usecase/SetDeviceLog";
import { IDeviceConnection } from "../devices/IDeviceConnection";

const option = 
    {
        machinName:"A-1",
        url:"opc.tcp://device-mock:26543",
        value:[
            {
                name:"Temperture ",
                nodeId:"ns=1;s=Temperature",
                field:"Temperature"
            },
            {
                name:"Condition Message",
                nodeId:"ns=1;i=1046",
                field:"Message"
            }
        ],
        event:[
            {
                name:"Temperture Condition Event",
                nodeId:"ns=1;i=1001",
                fields:[
                    "ReceiveTime",
                    "Message"
                ]
            }
        ]
    };


export class DeviceController {
    private usecase:ISetDeviceLog;
    private deviceConnection:IDeviceConnection;

    constructor(usecase:ISetDeviceLog, deviceConection:IDeviceConnection){
        this.usecase = usecase;
        this.deviceConnection = deviceConection;
    }

    async execute(options:any){
        this.connection(option.url).then(async () =>{
            option.value.forEach((value)=>{
                this.subscribeValue(value.nodeId, value.field);
            });
            option.event.forEach((value)=>{
                this.subscribeEvent(value.nodeId, value.fields);
            });
        });
    }

    private async connection(url:string):Promise<any> {
        await this.deviceConnection.connect(url);
    }

    private subscribeValue(nodeId:string, field:string){
        this.deviceConnection.subscriptionValue(nodeId,field,(deviceLog)=>{
            this.usecase.execute(deviceLog);
        });
    }
    private subscribeEvent(nodeId:string, fields:Array<string>){
        this.deviceConnection.subscriptionEvent(nodeId,fields,(deviceLog)=>{
            this.usecase.execute(deviceLog);
        });
    }
}