import { OpcuaConnection } from "./infrastructure/devices/OpcuaConnection"
import { DeviceController } from "./interface/controllers/DeviceController"
import { SetDeviceLog } from "./application/usecase/SetDeviceLog";
import { DeviceLogRepository } from "./interface/database/DeviceLogRepository";
import { RedisConnection } from "./infrastructure/database/RedisConnection";

let controller = new DeviceController(new SetDeviceLog(new DeviceLogRepository(new RedisConnection())), new OpcuaConnection());

controller.execute("a");
//const opcua = new OpcuaConnection();
//opcua.connect("opc.tcp://device-mock:26543").then(async () =>{
//    const value = await opcua.read("ns=1;s=Temperature");
//    console.log("data:",value);
//
//    await opcua.subscriptionValue("ns=1;s=Temperature","Temprature",(deviceLog)=>{
//        console.log(deviceLog);
//    });
//    await opcua.subscriptionValue("ns=1;i=1046","Condition Message",(deviceLog)=>{
//        console.log(deviceLog);
//    });
//    var fields = [
//        "ReceiveTime",
//        "Message"
//    ];
//    await opcua.subscriptionEvent("ns=1;i=1001",fields,(deviceLog)=>{
//        console.log(deviceLog);
//    });
//
//});
