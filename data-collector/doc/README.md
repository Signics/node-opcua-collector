### アーキテクチャ構成
- クリーンアーキテクチャに習う形とする。
![](img/clean_arch.jpeg)

### 各層の役割
|レイヤー|責務|
|:--|:--|
|domain|ビジネスルールのための、データ構想、メソッドを物オブジェクトが入る。|
|application|アプリケーションのユースケースを表現する。|
|interface|infrastructureと内部ユースケースの層との間を繋ぐ役割。データモデルの変換をおこなう|
|infrastructure|フレームワークやツール、DBなどから構成されるレイヤー|

```plantuml
@startuml
namespace domain #FFFF00 {
  namespace models  {
    class DeviceLog
  }
}

namespace application #FFBBBB {
  namespace repositories {
    interface IDeviceLogRepository
  }
  namespace usecase {
    interface ISetDeviceLog
    class SetDeviceLog

    SetDeviceLog --|> ISetDeviceLog
    SetDeviceLog --> domain.models.DeviceLog
    SetDeviceLog --> application.repositories.IDeviceLogRepository
  }
}

namespace interface #54FFDD {
  namespace devices {
    interface IDeviceConnection
  }
  namespace controller {
    class DeviceController

    DeviceController --> interface.devices.IDeviceConnection
    DeviceController --> application.usecase.ISetDeviceLog
  }
  namespace database {
    class DeviceLogRepository
    interface INoSqlConnection

    DeviceLogRepository --> INoSqlConnection

    DeviceLogRepository --|> application.repositories.IDeviceLogRepository
  }
}

namespace infrastructure #55CCFF{
  namespace database {
    class RedisConnection

    RedisConnection --|> interface.database.INoSqlConnection
  }
  namespace devices {
    class OpcuaConnection
    
    OpcuaConnection --|> interface.devices.IDeviceConnection
  }
}
@enduml
```

