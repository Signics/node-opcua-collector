### 概要
- データ収集 イメージ 
### Install
```
docker-compose build
docker-compose run data-collector /bin/sh
npm install
```
### ビルド
```
docker-compose run --service-ports data-collector npm run build 
```
### デバッグ起動手順	
```
docker-compose run --service-ports data-collector npm run debug 
```

### ドキュメント
本リポジトリのドキュメントは以下で管理する。
- [doc](doc)

